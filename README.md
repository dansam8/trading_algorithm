# Trading algorithm - exercises

## Tasks

The details for the tasks are in their respective packages.

## Install

To run the code in this repository clone it and install the requirements.

    git clone https://dansam8@bitbucket.org/dansam8/trading_algorithm.git
    cd trading_algorithm
    pip install -r requirements.txt

All code will run without swifter, it is just used to parallelize the prepossessing of the data.
    
## Data

Both exercises rely on the data created by Data_handling. Make sure to run `Data_handling/data_handling.py`
before attempting to run them. This will download and preprocess the data.
       
