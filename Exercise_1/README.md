# Exercise_1

## Running the code:

Make sure the data has been downloaded and preprocessed by running `Data_handling/data_handling.py`
Then run `trade_rule.py`

## All results show here are from AUDUSD

## E1.1
For this sub-task the trading rule was implemented and the optimal stoploss was searched for using the validation
(train) dataset. 

### search outcome render
![alt text](results/AUDUSD_stoploss_search.png)

### profit distribution val / train profits
![alt text](results/AUDUSD_strategy_one_val_profits_distribution.png)

### profit distribution test profits
![alt text](results/AUDUSD_strategy_one_test_profits_distribution.png)

### results

    Strategy one, val data: 
    mean profit: 0.000173, std: 0.000628, percent profitable: 85.426945
    Strategy one, test data: 
    mean profit: 0.000017, std: 0.001913, percent profitable: 51.747088

## E1.2
For this sub-task E1.1 was adapted to search for the optimal stop-loss and take profit.
For this a grid search was used
### search outcome render
Y axis = stop loss, X axis = take profit, color = mean profit

![alt text](results/AUDUSD_takeprofit_stoploss_search.png)

### profit distribution val / train profits
![alt text](results/AUDUSD_strategy_two_val_profits_distribution.png)

### profit distribution test profits
![alt text](results/AUDUSD_strategy_two_test_profits_distribution.png)

### results
    Strategy two, val data: 
    mean profit: 0.000255, std: 0.001670, percent profitable: 59.704433
    Strategy two, test data: 
    mean profit: 0.000322, std: 0.001555, percent profitable: 62.958115