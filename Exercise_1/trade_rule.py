import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from tqdm import tqdm
import os
import warnings

MAX_TRADE_CANDLESTICKS = 60 * 24 * 3  # three days
DAY_IN_SECONDS = 60 * 60 * 24
CANDLESTICK_LENGTH = 60  # in seconds

DATA_FOLDER = "../Data_handling/data"
EXAMPLE_PAIRS = ["AUDUSD", "EURGBP", "EURUSD"]

warnings.simplefilter(action='ignore', category=FutureWarning)


def get_trading_space(df):

    """
    :return an np array of shape [opens, max trade length] where axis 1 contains daily close followed by the next
    MAX_TRADE_TIME of candlesticks
    """
    time_span = df["<TIMESTAMP>"].min(), df.iloc[:-MAX_TRADE_CANDLESTICKS]["<TIMESTAMP>"].max()

    first_daily_close = time_span[0] + DAY_IN_SECONDS - time_span[0] % DAY_IN_SECONDS - CANDLESTICK_LENGTH
    last_daily_close = time_span[1] - (time_span[1] % DAY_IN_SECONDS) - CANDLESTICK_LENGTH

    # assuming that close time is 23:59:00 as the data has prices for all 24 hours
    daily_close_times = pd.DataFrame({"<TIMESTAMP>": np.array(range(first_daily_close, last_daily_close, DAY_IN_SECONDS))})
    daily_close_times = df.merge(daily_close_times, on="<TIMESTAMP>", how="inner")  # inner join so that we drop weekend open times
    daily_close_times = daily_close_times["<TIMESTAMP>"].values

    # trading space is an array of size [total opens x three days with of candle sticks]
    trading_space = []
    for open_time in daily_close_times:
        index_of_close = df.index[df["<TIMESTAMP>"] == open_time][0]
        trading_space.append(df[index_of_close: index_of_close + MAX_TRADE_CANDLESTICKS]["<OPEN>"].values)

    trading_space = np.stack(trading_space)

    return trading_space


def execute_trade_rule(trading_space, stop_loss, take_profit=0):

    """
    Executes the trade rule outlined in exercise 1
    Fulfills the requirements of E1 1 while take profit is zero and the requirements of E1 2 when take profit is set
    above zero

    :return mean profit as a ratio
    """

    # apply trading rule
    exits = (trading_space[:, 1:] > trading_space[:, 0:1] * (1 + take_profit)) | \
            (trading_space[:, 1:] < trading_space[:, 0:1] * (1 - stop_loss))

    exits[:, -1] = True  # force exit at end of trading space

    exit_indices = exits.argmax(axis=1)

    entry_price = trading_space[:, 0]
    exit_price = trading_space[:, 1:][np.arange(exits.shape[0]), exit_indices]

    profits = exit_price - entry_price

    return profits


def search_for_optimal_stop_loss(pair, search_range=(0, 0.03), search_size=30):

    df = pd.read_csv(os.path.join(DATA_FOLDER, pair + "_train.csv"))
    trading_space = get_trading_space(df)

    search_space = np.arange(search_size) / (search_size / (search_range[1] - search_range[0])) + search_range[0]

    mean_profits = []
    for stop_loss_test in tqdm(search_space):

        mean_profit = execute_trade_rule(trading_space, stop_loss=stop_loss_test).mean()
        mean_profits.append(mean_profit)

    plt.plot(search_space, mean_profits)
    plt.xlabel("stop loss")
    plt.ylabel("mean profit")
    plt.tight_layout()
    plt.savefig("results/" + pair + "_stoploss_search")
    plt.clf()

    optimal_stop_loss = search_space[np.argmax(mean_profits)]

    return optimal_stop_loss


def grid_search_of_optimal_stop_loss_and_take_profit(pair, search_range=(0, 0.03), search_size=30):

    df = pd.read_csv(os.path.join(DATA_FOLDER, pair + "_train.csv"))
    trading_space = get_trading_space(df)
    search_space = np.arange(search_size) / (search_size / (search_range[1] - search_range[0])) + search_range[0]
    outcomes = np.empty([search_size, search_size])

    for i, stop_loss in enumerate(tqdm(search_space)):
        for k, take_profit in enumerate(search_space):
            mean_profit = execute_trade_rule(trading_space, stop_loss=stop_loss, take_profit=take_profit).mean()

            outcomes[i, k] = mean_profit

    index_of_max = np.unravel_index(np.argmax(outcomes, axis=None), outcomes.shape)
    optimal_stop_loss = search_space[index_of_max[0]]
    optimal_take_profit = search_space[index_of_max[1]]

    sns.heatmap(outcomes, xticklabels=search_space, yticklabels=search_space)
    plt.savefig("results/" + pair + "_takeprofit_stoploss_search")
    plt.clf()

    return {"optimal_stop_loss": optimal_stop_loss, "optimal_take_profit": optimal_take_profit}


def evaluate(pair):

    val_data = os.path.join(DATA_FOLDER, pair + "_train.csv")
    test_data = os.path.join(DATA_FOLDER, pair + "_test.csv")

    def print_stats(title, profits):
        print(pair, title, "\n", "mean profit: %f, std: %f, percent profitable: %f" % (profits.mean(),
              profits.std(0),  (profits.clip(min=0).sum() / np.abs(profits).sum()) * 100))

    df_val = pd.read_csv(val_data)
    trading_space_val = get_trading_space(df_val)

    df_test = pd.read_csv(test_data)
    trading_space_test = get_trading_space(df_test)

    # strategy one
    optimal_stoploss = search_for_optimal_stop_loss(pair=pair)
    print("optimal stop loss", optimal_stoploss)

    profits = execute_trade_rule(trading_space_val, stop_loss=optimal_stoploss)
    sns.distplot(profits, axlabel="profit")
    print_stats("Strategy one, val data:", profits)
    plt.savefig("results/" + pair + "_strategy_one_val_profits_distribution")
    plt.clf()

    profits = execute_trade_rule(trading_space_test, stop_loss=optimal_stoploss)
    sns.distplot(profits, axlabel="profit")
    print_stats("Strategy one, test data:", profits)
    plt.savefig("results/" + pair + "_strategy_one_test_profits_distribution")
    plt.clf()

    # strategy two
    optimal_parameters = grid_search_of_optimal_stop_loss_and_take_profit(pair=pair)
    print(optimal_parameters)

    profits = execute_trade_rule(trading_space_val, stop_loss=optimal_parameters["optimal_stop_loss"],
                                 take_profit=optimal_parameters["optimal_take_profit"])
    sns.distplot(profits, axlabel="profit")
    print_stats("Strategy two, val data:", profits)
    plt.savefig("results/" + pair + "_strategy_two_val_profits_distribution")
    plt.clf()

    profits = execute_trade_rule(trading_space_test, stop_loss=optimal_parameters["optimal_stop_loss"],
                                 take_profit=optimal_parameters["optimal_take_profit"])
    sns.distplot(profits, axlabel="profit")
    print_stats("Strategy two, test data:", profits)
    plt.savefig("results/" + pair + "_strategy_two_test_profits_distribution")
    plt.clf()


def main():
    for pair in EXAMPLE_PAIRS:
        evaluate(pair)


if __name__ == '__main__':
   main()