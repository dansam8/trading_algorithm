import torch
from torch import nn


class BasicGRU(nn.Module):

    def __init__(self, hidden_size):
        super(BasicGRU, self).__init__()
        self.gru = nn.GRU(1, hidden_size, num_layers=1, batch_first=True)
        self.fcl = nn.Linear(hidden_size, 1)
        self.activation = nn.Tanh()

    def forward(self, x, working_len):
        _, h = self.gru(x.unsqueeze(-1), None)
        output = torch.empty(x.size(0), working_len, device=x.device)
        next_input = x[:, -1]

        for i in range(working_len):
            yi, h = self.gru(next_input[:, None, None], h)
            yi = self.fcl(self.activation(yi))
            output[:, i] = yi[:, 0, 0]
            next_input = yi[:, 0, 0]
        return output


class AttentionLayer(nn.Module):
    def __init__(self, hidden_size, attention_hidden_size):
        super(AttentionLayer, self).__init__()
        self.w1 = nn.Linear(hidden_size, attention_hidden_size)
        self.w2 = nn.Linear(hidden_size, attention_hidden_size)
        self.w3 = nn.Linear(attention_hidden_size * 2, 1)
        self.activation = nn.Tanh()

    def forward(self, hiddens):
        w1 = self.activation(self.w1(hiddens[:, -1]))
        w1 = w1[:, None, :].expand(-1, hiddens.size(1), -1)
        w2 = self.activation(self.w2(hiddens))
        w3 = self.w3(torch.cat((w1, w2), dim=2))
        attention_scores = torch.softmax(w3.squeeze(-1), dim=1)
        return attention_scores


class GRUWithAttention(nn.Module):
    def __init__(self, hidden_size, attention_hidden_size):
        super(GRUWithAttention, self).__init__()
        self.gru = nn.GRU(1, hidden_size, num_layers=1, batch_first=True)
        self.attention = AttentionLayer(hidden_size, attention_hidden_size)
        self.fcl = nn.Linear(hidden_size, 1)
        self.funnel = nn.Linear(hidden_size * 2, hidden_size)
        self.activation = nn.Tanh()

    def forward(self, x, working_len):

        hiddens, hidden = self.gru(x.unsqueeze(-1))  # encode context
        outputs = torch.empty(x.size(0), working_len, device=x.device)
        next_input = x[:, -1]

        for i in range(working_len):

            attention_scores = self.attention(hiddens)
            context_vector = (hiddens * attention_scores.unsqueeze(-1)).sum(1)
            # get last hidden and context vector from 2x hidden_size to 1x hidden_size
            context_vector = self.activation(self.funnel(torch.cat((context_vector, hiddens[:, -1]), dim=1)))

            _, hidden = self.gru(next_input[:,  None, None], context_vector[None])  # pass context vector as hidden
            hiddens = torch.cat((hiddens, hidden[0, :, None]), dim=1)  # append hiddens
            outputs[:, i] = self.fcl(hidden[0]).squeeze(1)  # get prediction

        return outputs
