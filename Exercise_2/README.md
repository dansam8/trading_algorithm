# Exercise_2

## Running the code

Make sure the data has been downloaded and preprocessed by running `Data_handling/data_handling.py`
Then run `testing.py`

## Goal
To train two model to predict the movements of a currency pair and then compare them

## Models

The chosen models to compare were a vanilla GRU (gated recurrent unit), and a GRU with an attention layer.
These models where trained to predict the next 60 time steps given a context window of 180 time steps, using the close
price of one minute candlestick data. The models were trained for 4000 iterations given the following hyper parameters
    
    CONTEXT_LEN = 3 * 60
    WORKING_LEN = 1 * 60
    LR = 1e-4  # learning rate
    MODEL_HIDDEN_SIZE = 256
    MODEL_ATTENTION_HIDDEN_SIZE = 32  # for the model with attention
    BATCH_SIZE = 128

## results
### basic GRU
    model: models/basic_gru_4000
    mean error: 0.2365

![alt text](figs/basic_gru_4000_error_across_time_steps.png)
![alt text](figs/basic_gru_4000_prediction_example.png)

### GRU with attention
    model: models/gru_with_attention_1000
    mean error: 0.2311

![alt text](figs/gru_with_attention_4000_error_across_time_steps.png)
![alt text](figs/gru_with_attention_4000_prediction_example.png)

## conclusion

The GRU with attention layer seamed to perform sightly better on the task. The error is slightly lower and model 
started converging sooner. However, this could just be due to the lager amount of parameters. It is still unclear
weather using attention with a GRU for generating future price predictions has any advantage over the same model without
attention.