import torch
from torch import nn
from torch.utils.data import DataLoader
from tqdm import tqdm
from torch.utils.tensorboard import SummaryWriter
import os

from Exercise_2.data_sets import ForexSet
from Exercise_2.models import BasicGRU, GRUWithAttention

DATA_PATH = "../Data_handling/data/AUDUSD_train.csv"
TEST_DATA_PATH = "../Data_handling/data/AUDUSD_test.csv"
MODEL_DIR = "models"

CONTEXT_LEN = 3 * 60
WORKING_LEN = 1 * 60
LR = 1e-4
MODEL_HIDDEN_SIZE = 256
MODEL_ATTENTION_HIDDEN_SIZE = 32
BATCH_SIZE = 128

torch.autograd.set_detect_anomaly(True)


def train_future_price_generation(run_name, model, batch_size, max_iterations, device, save_freq=1000):

    assert device[:4] != "cuda" or torch.cuda.is_available(), "device is cuda but cuda is not available"

    dataset = ForexSet(DATA_PATH, context_len=CONTEXT_LEN, working_len=WORKING_LEN)
    dataloader = DataLoader(dataset, batch_size=batch_size, num_workers=2, shuffle=True)
    model = model.to(device)
    opt = torch.optim.Adam(model.parameters(), lr=LR)
    loss_fn = nn.MSELoss()

    summary_writer = SummaryWriter("tb/" + run_name)

    for b_id, x in enumerate(tqdm(dataloader)):

        if b_id > max_iterations:
            break

        x = x.to(device)

        context_data = x[:, :CONTEXT_LEN]
        working_data = x[:, CONTEXT_LEN:]

        yi = model.forward(context_data, WORKING_LEN)
        loss = loss_fn(yi, working_data)

        opt.zero_grad()
        loss.backward()
        opt.step()

        summary_writer.add_scalar("mean error", (yi - working_data).abs().mean().item(), b_id)
        summary_writer.add_scalar("loss", loss, b_id)
        summary_writer.add_scalar("output var", yi.var().item(), b_id)

        if b_id % save_freq == 0:
            torch.save(model, os.path.join(MODEL_DIR, run_name + "_" + str(b_id)))


if __name__ == '__main__':

    train_future_price_generation(
        "basic_gru",
        BasicGRU(MODEL_HIDDEN_SIZE),
        batch_size=BATCH_SIZE, max_iterations=4000, device="cuda")

    train_future_price_generation(
        "gru_with_attention",
        GRUWithAttention(hidden_size=MODEL_HIDDEN_SIZE, attention_hidden_size=MODEL_ATTENTION_HIDDEN_SIZE),
        batch_size=BATCH_SIZE, max_iterations=4000, device="cuda")
