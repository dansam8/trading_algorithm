import torch
from torch.utils.data import DataLoader
import os
import matplotlib.pyplot as plt
import numpy as np

from Exercise_2.data_sets import ForexSet
from Exercise_2.models import BasicGRU, GRUWithAttention

TEST_DATA_PATH = "../Data_handling/data/AUDUSD_test.csv"

WORKING_LEN = 1 * 60
CONTEXT_LEN = 3 * 60
FIG_OUTPUT = "figs"
SAMPLE_SIZE = 10_000


def test_sequence_generation_model(model_path, sample_size):
    torch.random.manual_seed(42)
    np.random.seed(42)

    dataset = ForexSet(TEST_DATA_PATH, context_len=CONTEXT_LEN, working_len=WORKING_LEN)
    dataloader = DataLoader(dataset, shuffle=True, batch_size=sample_size)
    model = torch.load(model_path, map_location="cpu")

    data = next(iter(dataloader))
    context, working_data = data[:, :CONTEXT_LEN], data[:, CONTEXT_LEN:]

    with torch.no_grad():
        prediction = model(context, WORKING_LEN)

    print(
        "model:", model_path + "\n"
        "mean error:", str((prediction - working_data).abs().mean().item())[:6], "\n"
    )

    plt.plot(list(range(WORKING_LEN)), (working_data - prediction).abs().mean(0).numpy(),
             label="error across time steps")
    plt.xlabel("Time steps")
    plt.ylabel("Error")
    plt.legend()
    plt.savefig(os.path.join(FIG_OUTPUT, model_path.split("/")[-1] + "_error_across_time_steps"))
    plt.clf()

    plt.plot(list(range(CONTEXT_LEN)), context[0].numpy(), label="context")
    plt.plot(list(range(CONTEXT_LEN, WORKING_LEN + CONTEXT_LEN)), prediction[0].numpy(), label="prediction")
    plt.plot(list(range(CONTEXT_LEN, WORKING_LEN + CONTEXT_LEN)), working_data[0].numpy(), label="actual")
    plt.xlabel("Time steps")
    plt.ylabel("Scaled price")
    plt.legend()
    plt.savefig(os.path.join(FIG_OUTPUT, model_path.split("/")[-1] + "_prediction_example"))
    plt.clf()


if __name__ == '__main__':

    test_sequence_generation_model("models/basic_gru_4000", sample_size=SAMPLE_SIZE)
    test_sequence_generation_model("models/gru_with_attention_4000", sample_size=SAMPLE_SIZE)
