import torch
from torch import nn
import pandas as pd
import numpy as np


def scale_data(data, context_len):

    """
    :returns the price data scaled between zero and one for the context_data as well as a tuple containing the scale
     and shift.
    Note the working data may not fit this scale as including that when scaling would give the model information about
    the future price movements.
    """

    if type(data) is np.ndarray:
        data = torch.from_numpy(data.astype(float)).float()

    shift = data[:context_len].min()

    data -= shift

    scale = torch.empty_like(data)

    scale[:] = (
            (
                    data[:context_len].max() -
                    data[:context_len].min()
            )
    ) if data[:context_len].max() != data[:context_len].min() else 1

    data /= scale

    return data, (float(scale[0]), float(shift))


class ForexSet(nn.Module):
    def __init__(self, csv_path, context_len, working_len):
        super(ForexSet, self).__init__()

        self.df = pd.read_csv(csv_path)

        self.context_len = context_len
        self.working_len = working_len

    def __len__(self):
        return self.df.shape[0] - self.context_len - self.working_len

    def __getitem__(self, item):

        section = self.df.iloc[item: item + self.context_len + self.working_len]
        prices = section["<CLOSE>"].values
        scaled_prices, (scale, shift) = scale_data(prices, self.context_len)
        return scaled_prices


def basic_test():
    import matplotlib.pyplot as plt

    CONTEXT_LEN = 3 * 60
    WORKING_LEN = 3 * 60

    forex_set = ForexSet("../Data_handling/data/AUDUSD_train.csv", context_len=CONTEXT_LEN, working_len=WORKING_LEN)

    prices = forex_set.__getitem__(0)
    prices = forex_set.__getitem__(len(forex_set) - 1)

    assert prices.size(1) == CONTEXT_LEN + WORKING_LEN, "prices total len not equal to CONTEXT_LEN + WORKING_LEN"

    plt.plot(list(range(CONTEXT_LEN)), prices[0, :CONTEXT_LEN].numpy(), label="context_data")
    plt.plot(list(range(CONTEXT_LEN, CONTEXT_LEN + WORKING_LEN)), prices[0, WORKING_LEN:].numpy(), label="working_data")
    plt.legend()
    plt.show()


if __name__ == '__main__':
    basic_test()
