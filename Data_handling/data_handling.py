import pandas as pd
from datetime import datetime
import pytz
import os
from Data_handling.download_script import download_data
try:
    import swifter
except ImportError:
    print("not using swifter as it could not be imported")


TEST_TRAIN_SPLIT = 0.1
TIME_RANGE = 60 * 60 * 24 * 365 * 5  # 5 years of data
INPUT_FOLDER = "input_data"
OUTPUT_FOLDER = "data"
CANDLESTICK_LENGTH = 60  # seconds
EXAMPLE_PAIRS_TO_PROCESS = ["AUDUSD", "EURGBP", "EURUSD"]


def get_timestamps(df):

    """gets timestamp from date and time"""

    def f(df):

        date = datetime(year=int(df["<DTYYYYMMDD>"][:4]), month=int(df["<DTYYYYMMDD>"][4:6]),
                        day=int(df["<DTYYYYMMDD>"][6:8]), hour=int(df["<TIME>"][:2]), minute=int(df["<TIME>"][2:4]),
                        second=int(df["<TIME>"][4:6]), tzinfo=pytz.timezone("gmt"))

        return int(date.timestamp())

    if "swifter" in globals():
        timestamps = df.swifter.apply(f, axis=1)
    else:
        timestamps = df.apply(f, axis=1)

    return timestamps


def preprocess_pair_data(pair):

    path = os.path.join(INPUT_FOLDER, pair + ".csv")

    if not os.path.exists(path):
        download_data(pair)

    print("converting time stamps for " + pair + " this will take a while")
    df = pd.read_csv(path, dtype={"<DTYYYYMMDD>": str, "<TIME>": str})
    df["<TIMESTAMP>"] = get_timestamps(df)

    # trim to needed time range
    df = df[df["<TIMESTAMP>"] > df["<TIMESTAMP>"].max() - TIME_RANGE]

    split_point = int(df.shape[0] * (1 - TEST_TRAIN_SPLIT))
    df_train, df_test = df.iloc[:split_point], df.iloc[split_point:]

    if not os.path.exists(OUTPUT_FOLDER):
        os.mkdir(OUTPUT_FOLDER)

    df_train.to_csv(os.path.join(OUTPUT_FOLDER, path.split("/")[-1].split(".")[0] + "_train.csv"))
    df_test.to_csv(os.path.join(OUTPUT_FOLDER,  path.split("/")[-1].split(".")[0] + "_test.csv"))


def main():

    for pair in EXAMPLE_PAIRS_TO_PROCESS:
        preprocess_pair_data(pair)


if __name__ == "__main__":
    main()
