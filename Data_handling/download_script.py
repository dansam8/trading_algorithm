import gdown
import os

OUTPUT_DIR = 'input_data'
DATA_LINKS = {
    "AUDUSD": "https://drive.google.com/uc?export=download&id=1K7sva3p_xnwyYuzqn5j0K_tuFUpRSNoO",
    "EURGBP": "https://drive.google.com/uc?export=download&id=1OxZxT2Nec1A3s41RdFXgqArbA2GElYGx",
    "EURUSD": "https://drive.google.com/uc?export=download&id=1bTRzn4qLnWsRMFCOk0odF7XI-QrICS5h"
}


def download_data(pair):

    if not os.path.exists(OUTPUT_DIR):
        os.mkdir(OUTPUT_DIR)

    url = DATA_LINKS[pair]
    output = os.path.join(OUTPUT_DIR, pair + '.csv')
    gdown.download(url, output, quiet=False)
